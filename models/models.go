package models

type Register struct {
	Timestamp       string `json:"timeStamp"`
	MerchantID      string `json:"iMid"`
	PaymentMethod   string `json:"payMethod"`
	Currency        string `json:"currency"`
	Amount          string `json:"amt"`
	MerchantRef     string `json:"referenceNo"`
	GoodsName       string `json:"goodsNm"`
	BuyerName       string `json:"billingNm"`
	BuyerPhone      string `json:"billingPhone"`
	BuyerEmail      string `json:"billingEmail"`
	BuyerAddress    string `json:"billingAddr"`
	BuyerCity       string `json:"billingCity"`
	BillingState    string `json:"billingState"`
	BillingPost     string `json:"billingPostCd"`
	BillingCountry  string `json:"billingCountry"`
	NotificationUrl string `json:"dbProcessUrl"`
	MerchantToken   string `json:"merchantToken"`
	CartData        string `json:"cartData"`
	InstmntType     int    `json:"instmntType"`
	InstmntMon      int    `json:"instmntMon"`
	RecurrOption    int    `json:"recurrOpt"`
	BankCode        string `json:"bankCd"`
}

type Result struct {
	TxId string `json:"txid"`
}