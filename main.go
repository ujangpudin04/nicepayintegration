package main

import (
	reg "codetest/controllers"

	"github.com/gin-gonic/gin"
)


func main(){
	gin.SetMode(gin.ReleaseMode)
    r := gin.New()
	r.POST("/registration",reg.RegistrationController)
    r.Run(":5001")
}


// func RegistrationController(c *gin.Context) {
// 	file, err := os.OpenFile("logs.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	log.SetOutput(file)

// 	fmt.Println("Registration Process...")
// 	log.Println("Registration Process...")
// 	timenow := time.Now()
// 	timeStamp := timenow.Format("20060102150405")
// 	iMid := "IONPAYTEST"
// 	payMethod := "02"
// 	currency := "IDR"
// 	amt := "100"
// 	referenceNo := "ord" + timeStamp
// 	goodsNm := "Transaction Nicepay"
// 	billingNm := "John Doe"
// 	billingPhone := "02110680000"
// 	billingEmail := "email@merchant.com"
// 	billingAddr := "Jalan Bukit Berbunga 22"
// 	billingCity := "Jakarta"
// 	billingState := "DKI Jakarta"
// 	billingPostCd := "12345"
// 	billingCountry := "Indonesia"
// 	dbProcessUrl := "https://ptsv2.com/t/test-nicepay-v2"
// 	cartData := ""
// 	instmntType := 2
// 	instmntMon := 1
// 	recurrOpt := 0
// 	bankCd := "CENA"
// 	merchantKey := "33F49GnCMS1mFYlGXisbUDzVf2ATWCl9k3R++d5hDd3Frmuos/Xlx8XhXpe+LDYAbpGKZYSwtlyyLOtS/8aD7A=="
// 	mToken := timeStamp + iMid + referenceNo + amt + merchantKey
// 	h := sha256.New()
// 	h.Write([]byte(mToken))
// 	merchantToken := hex.EncodeToString(h.Sum(nil))

// 	fmt.Println("Request Data...")
// 	log.Println("Request Data...")
// 	register := data.Register{timeStamp, iMid, payMethod, currency, amt, referenceNo, goodsNm, billingNm, billingPhone, billingEmail, billingAddr, billingCity, billingState, billingPostCd, billingCountry, dbProcessUrl, merchantToken, cartData, instmntType, instmntMon, recurrOpt, bankCd}
// 	jsonReq, err := json.Marshal(register)
// 	bytesReq := bytes.NewBuffer(jsonReq)
// 	fmt.Println(bytesReq)
// 	log.Println(bytesReq)

// 	fmt.Println("Response Data...")
// 	log.Println("Response Data...")
// 	resp, err := http.Post("https://dev.nicepay.co.id/nicepay/direct/v2/registration", "application/json; charset=utf-8", bytesReq)
// 	if err != nil {
// 		log.Fatalln(err)
// 	}
// 	defer resp.Body.Close()
// 	bodyBytes, _ := ioutil.ReadAll(resp.Body)
// 	bodyString := string(bodyBytes)
// 	fmt.Println(bodyString)
// 	log.Println(bodyString)

// 	fmt.Println("Get TxId...")
// 	log.Println("Get TxId...")
// 	data := data.Result{}
// 	json.Unmarshal([]byte(bodyString), &data)
// 	fmt.Printf("TxId: %s", data.TxId)
// 	log.Printf("TxId: %s", data.TxId)

// 	fmt.Scanln()
// 	fmt.Println("done")

// }